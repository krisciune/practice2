package com.infobalt;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

public class WorkersView {

	private WorkersModel workersModel;

	public void create() {
		workersModel.setWorkerToEdit(new Worker());
	}

	public void update(Integer id) {
		workersModel.setWorkerToEdit(workersModel.searchWorkerById(id));
	}

	public void delete(Integer id) {
		workersModel.getWorkers().remove(workersModel.searchWorkerById(id));
		workersModel.setWorkerToEdit(null);
		message("Worker removed");
	}

	public void cancel() {
		workersModel.setWorkerToEdit(null);
	}

	public void save() {
		Integer id = workersModel.getWorkerToEdit().getId();
		if (id != null && id != 0) {
			workersModel.searchWorkerById(id)
					.setName(workersModel.getWorkerToEdit().getName());
			workersModel.searchWorkerById(id)
					.setSurname(workersModel.getWorkerToEdit().getSurname());
			message("Worker updated");
		} else {
			workersModel.getWorkerToEdit().setId(workersModel.getWorkers().size()+1);
			workersModel.getWorkers().add(workersModel.getWorkerToEdit());
			message("Worker added");
		}
		workersModel.setWorkerToEdit(null);
	}

	protected void message(String message) {
		FacesMessage doneMessage = new FacesMessage(message);
		FacesContext.getCurrentInstance().addMessage(null, doneMessage);
	}

	public WorkersModel getWorkersModel() {
		return workersModel;
	}

	public void setWorkersModel(WorkersModel workersModel) {
		this.workersModel = workersModel;
	}

}
