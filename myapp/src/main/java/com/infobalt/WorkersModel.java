package com.infobalt;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Named;

public class WorkersModel implements Serializable {

	private List<Worker> workers = new ArrayList<Worker>();
	private Worker workerToEdit = null;

	public void init() {
		workers.add(new Worker(1, "Petras", "Pavardenis"));
		workers.add(new Worker(2, "Jonas", "Pavardenis"));
		workers.add(new Worker(3, "Onute", "Pavarde"));

	}

	public Worker searchWorkerById(Integer id) {

		for (Worker worker : workers) {
			if (worker.getId() == id) {
				return worker;
			}
		}
		return null;
	}

	public List<Worker> getWorkers() {
		return workers;
	}

	public void setWorkers(List<Worker> workers) {
		this.workers = workers;
	}

	public Worker getWorkerToEdit() {
		return workerToEdit;
	}

	public void setWorkerToEdit(Worker workerToEdit) {
		this.workerToEdit = workerToEdit;
	}

}
